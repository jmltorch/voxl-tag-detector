cmake_minimum_required(VERSION 3.3)


set(TARGET voxl-inspect-tags)
add_executable(${TARGET} voxl-inspect-tags.c)

target_link_libraries(${TARGET}
	m
	/usr/lib64/libmodal_pipe.so
	/usr/lib64/libmodal_json.so
	/usr/lib64/libvoxl_cutils.so
)

install(
	TARGETS ${TARGET}
	LIBRARY			DESTINATION /usr/lib
	RUNTIME			DESTINATION /usr/bin
	PUBLIC_HEADER	DESTINATION /usr/include
)

